// Parker Willis - Alphabet Soup
// Thank you for this amazing opportunity!

const fs = require('fs');

// Function that does the actual testing
// Basic error handling and checks
function findWordsInGrid(filePath) {
    if (typeof filePath !== 'string') {
        console.error('Invalid filePath provided.');
        return;
    }

    try {
        const { grid, words } = parseInputFile(filePath);
        
        if (!grid || !words) {
            console.error('Invalid content in input file.');
            return;
        }

        words.forEach(word => searchInGrid(grid, word));
    } catch (error) {
        console.error('Error occurred:', error);
    }
}

// Read and parse the content of the input file
function parseInputFile(filePath) {
    const content = fs.readFileSync(filePath, 'utf8').trim();

    if (!content) return {};

    const [gridSizeLine, ...lines] = content.split('\n');
    const [rowCount] = gridSizeLine.split('x').map(Number);

    if (isNaN(rowCount)) return {};

    const grid = lines.slice(0, rowCount).map(line => line.split(' '));
    const words = lines.slice(rowCount);

    return { grid, words };
}

// Iterates over the grid to find the starting point of the word
function searchInGrid(grid, word) {
    for (let row = 0; row < grid.length; row++) {
        for (let col = 0; col < grid[0].length; col++) {
            if (searchFromPosition(grid, word, row, col)) return;
        }
    }
}

// Iterates over all possible directions to search for the word
function searchFromPosition(grid, word, startRow, startCol) {
    return getDirections().some(([dRow, dCol]) => {
        const [endRow, endCol] = searchInDirection(grid, word, startRow, startCol, dRow, dCol);
        if (endRow !== -1) {
            console.log(`${word} ${startRow}:${startCol} ${endRow}:${endCol}`);
            return true;
        }
        return false;
    });
}

// All possible search directions
function getDirections() {
    return [
        [0, 1], [1, 0], [1, 1], [1, -1],
        [0, -1], [-1, 0], [-1, -1], [-1, 1]
    ];
}

// Searches for the word in the specified direction
function searchInDirection(grid, word, row, col, dRow, dCol) {
    let i = row, j = col;
    for (const c of word) {
        if (!isPositionValid(grid, i, j) || grid[i][j] !== c) return [-1, -1];
        i += dRow;
        j += dCol;
    }
    return [i - dRow, j - dCol];
}

// Checks if the position is correct within the grid
function isPositionValid(grid, row, col) {
    return row >= 0 && row < grid.length && col >= 0 && col < grid[0].length;
}

// Test the input file
findWordsInGrid('Input.txt');
